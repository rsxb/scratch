package main

import "testing"

func TestMinOccuring(t *testing.T) {
	got, _ := MinOccuring("aabbb")
	if got != 'a' {
		t.Errorf("minOccuring(\"aabbb\") = '%c'; want 'a'", got)
	}

	got, _ = MinOccuring("aabbcdddeeff")
	if got != 'c' {
		t.Errorf("minOccuring(\"aabbcdddeeff\") = '%c'; want 'c'", got)
	}

	_, err := MinOccuring("")
	if err == nil {
		t.Error("minOccuring(\"\") should return err; got nil")
	}
}

func TestMaxOccuring(t *testing.T) {
	got, _ := MaxOccuring("aabbb")
	if got != 'b' {
		t.Errorf("maxOccuring(\"aabbb\") = '%c'; want 'b'", got)
	}

	got, _ = MaxOccuring("aabbcdddeeff")
	if got != 'd' {
		t.Errorf("maxOccuring(\"aabbcdddeeff\") = '%c'; want 'd'", got)
	}

	_, err := MaxOccuring("")
	if err == nil {
		t.Error("maxOccuring(\"\") should return err; got nil")
	}
}
