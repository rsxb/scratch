package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	var sb strings.Builder
	sb.WriteString("INSERT INTO country (name) VALUES\n")

	file, err := os.Open("input.txt")
	if err != nil {
		log.Panic("Error opening file for reading: ", err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		s := fmt.Sprintf(
			"\t('%s'),\n",
			strings.TrimSpace(scanner.Text()),
		)
		sb.WriteString(s)
	}
	if err = scanner.Err(); err != nil {
		log.Panic("Error reading file: ", err)
	}

	output := strings.TrimSuffix(sb.String(), ",\n")
	fmt.Println(output)
}
